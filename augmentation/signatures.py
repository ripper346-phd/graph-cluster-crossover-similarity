import numpy as np
import networkx as nx


def heat_kernel_signature(G, num_steps=100, k=None):
    """Compute heat kernel signature of a graph

    Args:
        G (networkX Graph): graph to analyze
        num_steps (int=100): time steps to do
        k (int=None): number of samples to take. If None or greater than number of nodes it is taken the latest

    Returns:
        np_array: heat kernel signature matrix
    """
    if not k or k > G.number_of_nodes():
        k = G.number_of_nodes()

    L = nx.laplacian_matrix(G)
    L = L.toarray()
    evals, evecs = np.linalg.eigh(L)

    t_min = abs(4.0 * np.log(10.0) / evals[-1])
    eval = 0
    i = 1
    while eval <= 0:
        eval = evals[i]
        i += 1
    t_max = abs(4.0 * np.log(10.0) / eval)
    t_interval = np.exp(np.linspace(np.log(t_min), np.log(t_max), num=num_steps, endpoint=True))
    hks = np.einsum('nk,kt->nt', np.square(abs(evecs))[:, :k], np.exp(-abs(evals)[:, np.newaxis] * t_interval)[:k, :])
    return hks
