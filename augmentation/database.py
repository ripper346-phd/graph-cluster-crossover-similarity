import psycopg2
import json
import time
from random import random
import numpy as np
from augmentation.graph_converter import graph_to_torch_blob, np_array_to_blob, blob_to_np_array, torch_blob_to_graph
from augmentation.numpy_encoder import NumpyEncoder


class GeneralDatabase:
    sql_columns = {}
    generic_column = {'param': lambda col: col, 'load': lambda value: value}

    def __init__(self, table_name, user, passwd, db, host="localhost", port=3306, use_pool=False, n_tries=5):
        """
        Initialize database connection.
        """
        self.user = user
        self.passwd = passwd
        self.db = db
        self.host = host
        self.port = port
        self.table_name = table_name
        self.use_pool = use_pool
        self.n_tries = n_tries
        if not use_pool:
            self.db_connection = self.connect()

    def connect(self):
        """
        Open a connection to database.
        :return: connection to database
        """
        return psycopg2.connect(user=self.user, password=self.passwd, database=self.db, host=self.host, port=self.port)

    def get_connection(self, tries=0):
        if self.use_pool:
            try:
                return self.connect()
            except psycopg2.OperationalError as err:
                # Aside connection errors due parameters it mitigates collisions when using thread pools trying several
                # times with a random pause between 0 and 10 seconds
                if tries < self.n_tries:
                    time.sleep(random() * 10)
                    return self.get_connection(tries + 1)
                raise err
        return self.db_connection

    def disconnect(self, connection=None):
        """
        Close database connection.
        """
        if connection:
            connection.close()
        else:
            self.db_connection.close()

    def get_new_cursor(self):
        cursor = None
        conn = self.get_connection()
        try:
            cursor = conn.cursor()
        except:
            conn.reconnect()
            cursor = conn.cursor()
        return cursor, conn

    def prepare_sql(self, table, columns, is_insert=True):
        sql = []
        params = ()
        for col in self.sql_columns[table].keys():
            if columns.get(col) is not None:
                sql.append(self.get_sql(col, is_insert))
                params += (self.sql_columns[table][col]['param'](columns[col]),)
        return (params, sql)

    def get_sql(self, name, is_insert=True, concat=False):
        if is_insert:
            return '{}'.format(name)
        if concat:
            return '{name} = CONCAT({name}, %s)'.format(name=name)
        return '{} = %s'.format(name)

    def execute_sql(self, sql, params):
        cursor, conn = self.get_new_cursor()
        cursor.execute(sql, params)
        conn.commit()
        cursor.close()
        if self.use_pool:
            self.disconnect(conn)


class ClusterDatabase(GeneralDatabase):
    def prepare_cluster_sql(self, columns, is_insert=True):
        return self.prepare_sql('cluster', columns, is_insert)

    def save_clusters(self, graph_id, num_clusters, *, graph=None, clusters=None, subgraphs=None, ordered_graph=None, ordered_adj=None, ordered_clusters=None, g_class=None, hks_clusters={}):
        params, sql_par = self.prepare_cluster_sql(
            {'clusters': clusters, 'graph': graph, 'subgraphs': subgraphs, 'ordered_graph': ordered_graph,
             'ordered_adj': ordered_adj, 'ordered_clusters': ordered_clusters, 'g_class': g_class, **hks_clusters},
            True)
        sql = 'INSERT INTO "{table_name}"(id, num_clusters, {sql_par}) VALUES (%s, %s, {params})'.format(
            table_name=self.table_name, sql_par=', '.join(sql_par), params=', '.join(['%s'] * len(sql_par)))
        self.execute_sql(sql, (graph_id, num_clusters) + params)

    def update_clusters(self, graph_id, num_clusters, *, graph=None, clusters=None, subgraphs=None, ordered_graph=None, ordered_adj=None, ordered_clusters=None, g_class=None, hks_clusters={}):
        columns = {'clusters': clusters, 'graph': graph, 'subgraphs': subgraphs, 'ordered_graph': ordered_graph,
                   'ordered_adj': ordered_adj, 'ordered_clusters': ordered_clusters, 'g_class': g_class, **hks_clusters}
        params, sql = self.prepare_cluster_sql(columns, False)
        sql = 'UPDATE "{table_name}" SET {sql_par} WHERE id = %s AND num_clusters = %s'.format(
            sql_par=', '.join(sql), table_name=self.table_name)
        self.execute_sql(sql, params + (graph_id, num_clusters))

    def check_graph_id_cluster(self, graph_id):
        sql = 'SELECT id FROM "{table_name}" WHERE id = %s'.format(table_name=self.table_name)
        cursor, conn = self.get_new_cursor()
        cursor.execute(sql, (graph_id,))
        results = cursor.fetchone()
        cursor.close()
        if self.use_pool:
            self.disconnect(conn)
        return results is not None

    def get_clusters(self, params=['clusters'], filter=[], num_clusters=None, offset=None, limit=None):
        sql = 'SELECT id, num_clusters, {params} FROM "{table_name}"'.format(
            table_name=self.table_name, params=', '.join(params))
        if len(filter) > 0:
            sql += 'WHERE 1 = 1'
            if 'no_hks_complete' in filter:
                sql += ' AND ({})'.format(' OR '.join(
                    ['hks_cluster{} IS NULL'.format(i) for i in range(num_clusters)]))
        if limit is not None:
            if offset is not None:
                sql += ' LIMIT {:d}, {:d}'.format(offset, limit)
            else:
                sql += ' LIMIT {:d}'.format(limit)
        cursor, conn = self.get_new_cursor()
        cursor.execute(sql)
        columns = cursor.description
        results = [{columns[index][0]: self.sql_columns['cluster'][columns[index][0]]['load'](column)
                    for index, column in enumerate(value)} for value in cursor.fetchall()]
        cursor.close()
        if self.use_pool:
            self.disconnect(conn)
        return results if results else None

    def get_graph_cluster(self, graph_id, num_clusters, params=['clusters']):
        sql = 'SELECT {params} FROM "{table_name}" WHERE id = %s AND num_clusters = %s'.format(
            table_name=self.table_name, params=', '.join(params))
        cursor, conn = self.get_new_cursor()
        cursor.execute(sql, (graph_id, num_clusters))
        columns = cursor.description
        results = [{columns[index][0]: self.sql_columns['cluster'][columns[index][0]]['load'](column)
                    for index, column in enumerate(value)} for value in cursor.fetchall()]
        cursor.close()
        if self.use_pool:
            self.disconnect(conn)
        return results[0] if results else None

    def get_clusters_count(self):
        sql = 'SELECT COUNT(*) FROM "{table_name}"'.format(table_name=self.table_name)
        cursor, conn = self.get_new_cursor()
        cursor.execute(sql)
        results = cursor.fetchall()
        cursor.close()
        if self.use_pool:
            self.disconnect(conn)
        return results[0][0] if results else 0


class SimilarityDatabase(GeneralDatabase):
    def prepare_similarity_sql(self, columns, is_insert=True):
        return self.prepare_sql('similarity', columns, is_insert)

    def prepare_similarity_insert_data(self, start_idx, compare_idx, n_clusters, similarity):
        similarity = similarity if type(similarity) is list else list(similarity)
        avg_similarity = sum(sum(similarity, [])) / (n_clusters * n_clusters)
        return (start_idx, compare_idx, n_clusters, json.dumps(similarity, cls=NumpyEncoder, separators=(',', ':')), avg_similarity)

    def save_similarity(self, graph1_id, graph2_id, num_clusters, similarity):
        similarity = similarity if type(similarity) is list else list(similarity)
        avg_similarity = sum(sum(similarity, [])) / (num_clusters * num_clusters)
        sql = 'INSERT INTO "{table_name}_similarity"(graph1_id, graph2_id, num_clusters, similarity, avg_similarity) VALUES (%s, %s, %s, %s, %s)'.format(
            table_name=self.table_name)
        self.execute_sql(sql, (graph1_id, graph2_id, num_clusters,
                               json.dumps(similarity, cls=NumpyEncoder, separators=(',', ':')), avg_similarity))

    def save_similarity_block(self, indices_to_save, n_clusters, data_container):
        block_size = 100
        indices_blocks = [
            tuple(
                values
                for start_idx, compare_idx, from_res in indices_to_save[x:x + block_size]
                for values in self.prepare_similarity_insert_data(start_idx, compare_idx, n_clusters, from_res.tolist()))
            for x in range(0, len(indices_to_save), block_size)
        ]
        cursor, conn = self.get_new_cursor()
        for block in indices_blocks:
            sql = ('INSERT INTO "{table_name}_similarity"(graph1_id, graph2_id, num_clusters, similarity, avg_similarity) VALUES '.format(table_name=self.table_name) +
                   "(%s, %s, %s, %s, %s)," * int(len(block) / 5))[:-1]
            cursor.execute(sql, block)
        conn.commit()
        cursor.close()
        if self.use_pool:
            self.disconnect(conn)

    def update_similarity(self, graph1_id, graph2_id, num_clusters, *, similarity=None, avg_similarity=None, combination=None, avg_combination=None):
        params, sql = self.prepare_similarity_sql(
            {'similarity': similarity, 'avg_similarity': avg_similarity, 'combination': combination, 'avg_combination': avg_combination}, False)
        sql = 'UPDATE "{table_name}_similarity" SET {} WHERE graph1_id = %s AND graph2_id = %s AND num_clusters = %s'.format(
            ', '.join(sql), table_name=self.table_name)
        self.execute_sql(sql, params + (graph1_id, graph2_id, num_clusters))

    def get_graph_similarity(self, graph_id, num_clusters, arrange_similarity=False):
        sql = 'SELECT graph1_id, graph2_id, similarity FROM "{table_name}_similarity" WHERE (graph1_id = %s OR graph2_id = %s) AND num_clusters = %s'.format(
            table_name=self.table_name)
        cursor, conn = self.get_new_cursor()
        cursor.execute(sql, (graph_id, graph_id, num_clusters))
        columns = cursor.description
        results = [{columns[index][0]: self.sql_columns['similarity'][columns[index][0]]['load'](column)
                    for index, column in enumerate(value)} for value in cursor.fetchall()]
        cursor.close()
        if self.use_pool:
            self.disconnect(conn)
        for idx in range(len(results)):
            if results[idx]['graph2_id'] == graph_id:
                results[idx]['graph2_id'], results[idx]['graph1_id'] = results[idx]['graph1_id'], results[idx]['graph2_id']
                if arrange_similarity:
                    results[idx]['similarity'] = np.transpose(results[idx]['similarity'])
        return {result['graph2_id']: result['similarity'] for result in results}

    def get_graph_similarities(self, limit=1000, offset=0, columns=[], ids=[]):
        sql = 'SELECT id, graph1_id, graph2_id {columns} FROM "{table_name}_similarity" {filter} LIMIT {offset}, {limit}'.format(
            table_name=self.table_name, offset=offset, limit=limit,
            columns=(', {}'.format(', '.join(columns)) if len(columns) else ''),
            filter=('WHERE id IN ({})'.format(','.join(map(str, ids))) if len(ids) else ''),
        )
        cursor, conn = self.get_new_cursor()
        cursor.execute(sql)
        columns = cursor.description
        results = [{columns[index][0]: self.sql_columns['similarity'][columns[index][0]]['load'](column)
                    for index, column in enumerate(value)} for value in cursor.fetchall()]
        cursor.close()
        if self.use_pool:
            self.disconnect(conn)
        return results

    def get_similarities_ids(self):
        sql = 'SELECT id FROM "{table_name}_similarity"'.format(table_name=self.table_name)
        cursor, conn = self.get_new_cursor()
        cursor.execute(sql)
        results = [int(value[0]) for value in cursor.fetchall()]
        cursor.close()
        if self.use_pool:
            self.disconnect(conn)
        return results


class AugmentedClusterDatabase(GeneralDatabase):
    def prepare_augmented_sql(self, columns, is_insert=True):
        return self.prepare_sql('augmented_cluster', columns, is_insert)

    def save_augmented_clusters(self, graph1_id, graph2_id, num_clusters, *, graph=None, clusters=None, subgraphs=None, ordered_graph=None, ordered_adj=None, ordered_clusters=None, g_class=None, hks_clusters={}):
        params, sql_par = self.prepare_augmented_sql(
            {'clusters': clusters, 'graph': graph, 'subgraphs': subgraphs, 'ordered_graph': ordered_graph,
             'ordered_adj': ordered_adj, 'ordered_clusters': ordered_clusters, 'g_class': g_class, **hks_clusters},
            True)
        sql = 'INSERT IGNORE INTO "{table_name}_augmented"(graph1_id, graph2_id, num_clusters, {sql_par}) VALUES (%s, %s, %s, {params})'.format(
            table_name=self.table_name, sql_par=', '.join(sql_par), params=', '.join(['%s'] * len(sql_par)))
        self.execute_sql(sql, (graph1_id, graph2_id, num_clusters) + params)

    def update_augmented_clusters(self, graph1_id, graph2_id, num_clusters, *, graph=None, clusters=None, subgraphs=None, ordered_graph=None, ordered_adj=None, ordered_clusters=None, g_class=None, hks_clusters={}):
        params, sql = self.prepare_augmented_sql(
            {'clusters': clusters, 'graph': graph, 'subgraphs': subgraphs, 'ordered_graph': ordered_graph,
             'ordered_adj': ordered_adj, 'ordered_clusters': ordered_clusters, 'g_class': g_class, **hks_clusters},
            False)
        sql = 'UPDATE "{table_name}_augmented" SET {sql_par} WHERE graph1_id = %s AND graph2_id = %s AND num_clusters = %s'.format(
            sql_par=', '.join(sql), table_name=self.table_name)
        self.execute_sql(sql, params + (graph1_id, graph2_id, num_clusters))

    def check_graph_id_augmented_cluster(self, graph1_id, graph2_id):
        sql = 'SELECT graph1_id, graph2_id FROM "{table_name}_augmented" WHERE (graph1_id = %s AND graph2_id = %s) OR (graph1_id = %s AND graph2_id = %s)'.format(
            table_name=self.table_name)
        cursor, conn = self.get_new_cursor()
        cursor.execute(sql, (graph1_id, graph2_id, graph2_id, graph1_id))
        results = cursor.fetchall()
        cursor.close()
        if self.use_pool:
            self.disconnect(conn)
        return results is not None and len(results) == 2

    def get_augmented_clusters(self, params=['clusters'], filter=[], offset=None, limit=None):
        sql = 'SELECT graph1_id, graph2_id, num_clusters, {params} FROM "{table_name}_augmented" {filter}'.format(
            table_name=self.table_name, params=', '.join(params),
            filter=('WHERE (graph1_id, graph2_id) IN ({})'.format(
                ','.join(['({}, {})'.format(f[0], f[1]) for f in filter])) if len(filter) else ''),)
        if limit is not None:
            if offset is not None:
                sql += ' LIMIT {:d}, {:d}'.format(offset, limit)
            else:
                sql += ' LIMIT {:d}'.format(limit)
        cursor, conn = self.get_new_cursor()
        cursor.execute(sql)
        columns = cursor.description
        results = [{columns[index][0]: self.sql_columns['augmented_cluster'][columns[index][0]]['load'](column)
                    for index, column in enumerate(value)} for value in cursor.fetchall()]
        cursor.close()
        if self.use_pool:
            self.disconnect(conn)
        return results if results else None

    def get_graph_augmented_cluster(self, graph1_id, graph2_id, num_clusters, params=['clusters']):
        sql = 'SELECT {params} FROM "{table_name}_augmented" WHERE graph1_id = %s AND graph2_id = %s AND num_clusters = %s'.format(
            table_name=self.table_name, params=', '.join(params))
        cursor, conn = self.get_new_cursor()
        cursor.execute(sql, (graph1_id, graph2_id, num_clusters))
        columns = cursor.description
        results = [{columns[index][0]: self.sql_columns['augmented_cluster'][columns[index][0]]['load'](column)
                    for index, column in enumerate(value)} for value in cursor.fetchall()]
        cursor.close()
        if self.use_pool:
            self.disconnect(conn)
        return results[0] if results else None


class Database(ClusterDatabase, SimilarityDatabase, AugmentedClusterDatabase, GeneralDatabase):
    def __init__(self, table_name, user, passwd, db, host="localhost", port=3306, use_pool=False, pool_name="executioner", n_tries=5):
        GeneralDatabase.__init__(self, table_name, user, passwd, db, host, port, use_pool, pool_name, n_tries)
        hks_cluster = {
            'param': lambda col: np_array_to_blob(col, True),
            'load': lambda value: blob_to_np_array(value, True) if value else None
        }
        self.sql_columns = {
            'cluster': {
                'id': GeneralDatabase.generic_column,
                'num_clusters': GeneralDatabase.generic_column,
                'graph': {
                    'param': lambda col: graph_to_torch_blob(col),
                    'load': lambda value: torch_blob_to_graph(value) if value else None
                },
                'g_class': GeneralDatabase.generic_column,
                'clusters': {
                    'param': lambda col: json.dumps(col if type(col) is list else list(col), cls=NumpyEncoder, separators=(',', ':')),
                    'load': lambda value: json.loads(value) if value else None
                },
                'subgraphs': {
                    'param': lambda col: graph_to_torch_blob(col),
                    'load': lambda value: torch_blob_to_graph(value) if value else None
                },
                'ordered_adj': {
                    'param': lambda col: np_array_to_blob(col, True),
                    'load': lambda value: blob_to_np_array(value, True) if value else None
                },
                'ordered_clusters': {
                    'param': lambda col: json.dumps(col if type(col) is list else list(col), cls=NumpyEncoder, separators=(',', ':')),
                    'load': lambda value: json.loads(value) if value else None
                },
                'ordered_graph': {
                    'param': lambda col: graph_to_torch_blob(col),
                    'load': lambda value: torch_blob_to_graph(value) if value else None
                },
                'hks_cluster0': hks_cluster,
                'hks_cluster1': hks_cluster,
                'hks_cluster2': hks_cluster,
                'hks_cluster3': hks_cluster,
                'hks_cluster4': hks_cluster,
                'hks_cluster5': hks_cluster,
                'hks_cluster6': hks_cluster,
                'hks_cluster7': hks_cluster,
                'hks_cluster8': hks_cluster,
                'hks_cluster9': hks_cluster,
            },
            'similarity': {
                'id': GeneralDatabase.generic_column,
                'graph1_id': GeneralDatabase.generic_column,
                'graph2_id': GeneralDatabase.generic_column,
                'num_clusters': GeneralDatabase.generic_column,
                'similarity': {
                    'param': lambda col: json.dumps(col if type(col) is list else list(col), cls=NumpyEncoder, separators=(',', ':')),
                    'load': lambda value: np.array(json.loads(value)) if value else None
                },
                'combination': {
                    'param': lambda col: json.dumps(col if type(col) is list else list(col), separators=(',', ':')),
                    'load': lambda value: json.loads(value) if value else None
                },
                'avg_similarity': GeneralDatabase.generic_column,
                'avg_combination': GeneralDatabase.generic_column,
            }
        }
        self.sql_columns['augmented_cluster'] = {
            'graph1_id': GeneralDatabase.generic_column,
            'graph2_id': GeneralDatabase.generic_column,
            'num_clusters': GeneralDatabase.generic_column,
            'graph': self.sql_columns['cluster']['graph'],
            'g_class': self.sql_columns['cluster']['g_class'],
            'clusters': self.sql_columns['cluster']['clusters'],
            'subgraphs': self.sql_columns['cluster']['subgraphs'],
            'ordered_adj': self.sql_columns['cluster']['ordered_adj'],
            'ordered_clusters': self.sql_columns['cluster']['ordered_clusters'],
            'ordered_graph': self.sql_columns['cluster']['ordered_graph'],
            'hks_cluster0': hks_cluster,
            'hks_cluster1': hks_cluster,
            'hks_cluster2': hks_cluster,
            'hks_cluster3': hks_cluster,
            'hks_cluster4': hks_cluster,
            'hks_cluster5': hks_cluster,
            'hks_cluster6': hks_cluster,
            'hks_cluster7': hks_cluster,
            'hks_cluster8': hks_cluster,
            'hks_cluster9': hks_cluster,
        }

    def create_table(self):
        """
        Check and create table for dataset
        """
        cursor, conn = self.get_new_cursor()
        sql = """
        CREATE TABLE IF NOT EXISTS "{table_name}" (
            id serial8 NOT NULL,
            num_clusters int4 NULL DEFAULT NULL,
            graph bytea NULL,
            g_class int4 NULL DEFAULT NULL,
            clusters text NULL,
            subgraphs bytea NULL,
            ordered_graph bytea NULL,
            ordered_adj bytea NULL,
            ordered_clusters text NULL,
            hks_cluster0 bytea NULL,
            hks_cluster1 bytea NULL,
            hks_cluster2 bytea NULL,
            hks_cluster3 bytea NULL,
            hks_cluster4 bytea NULL,
            hks_cluster5 bytea NULL,
            hks_cluster6 bytea NULL,
            hks_cluster7 bytea NULL,
            hks_cluster8 bytea NULL,
            hks_cluster9 bytea NULL,
            PRIMARY KEY (id, num_clusters)
        )""".format(table_name=self.table_name)
        cursor.execute(sql)
        sql = """
        CREATE TABLE IF NOT EXISTS "{table_name}_similarity" (
            id serial8 NOT NULL,
            graph1_id int4 NOT NULL,
            graph2_id int4 NOT NULL,
            num_clusters int4 NULL DEFAULT NULL,
            similarity text NULL,
            avg_similarity decimal(10, 5) NULL,
            combination text NULL,
            avg_combination decimal(10, 5) NULL,
            PRIMARY KEY (graph1_id, graph2_id, num_clusters)
        )""".format(table_name=self.table_name)
        cursor.execute(sql)
        sql = """CREATE INDEX IF NOT EXISTS "avg_similarity_idx" ON "{table_name}_similarity" USING btree ("avg_similarity")""".\
            format(table_name=self.table_name)
        cursor.execute(sql)
        sql = """CREATE INDEX IF NOT EXISTS "id" ON "{table_name}_similarity" USING btree ("id")""".\
            format(table_name=self.table_name)
        cursor.execute(sql)
        sql = """
        CREATE TABLE IF NOT EXISTS "{table_name}_augmented" (
            graph1_id int4 NOT NULL,
            graph2_id int4 NOT NULL,
            num_clusters int4 NULL DEFAULT NULL,
            graph bytea NULL,
            g_class int4 NULL DEFAULT NULL,
            clusters text NULL,
            subgraphs bytea NULL,
            ordered_graph bytea NULL,
            ordered_adj bytea NULL,
            ordered_clusters text NULL,
            hks_cluster0 bytea NULL,
            hks_cluster1 bytea NULL,
            hks_cluster2 bytea NULL,
            hks_cluster3 bytea NULL,
            hks_cluster4 bytea NULL,
            hks_cluster5 bytea NULL,
            hks_cluster6 bytea NULL,
            hks_cluster7 bytea NULL,
            hks_cluster8 bytea NULL,
            hks_cluster9 bytea NULL,
            PRIMARY KEY (graph1_id, graph2_id, num_clusters)
        )""".format(table_name=self.table_name)
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        if self.use_pool:
            self.disconnect(conn)
