from typing import Dict, List, Tuple
from augmentation.graph_converter import numpy_matrix_to_graph
from multiprocessing.pool import ThreadPool
from augmentation.draw import draw_adjacency_matrix
from augmentation.cluster import group_clusters
import numpy as np
from augmentation.signatures import heat_kernel_signature
from scipy.spatial.distance import cosine as cosine_distance
from augmentation.auction import auction_alg, flip_array
from augmentation.database import Database


def get_couples(graph1: Dict, graph2: Dict, clusters: Tuple[int, int]) -> List[List[int]]:
    """Perform the auction algorithm on the cosine similarity of the heat kernel signatures of the selected clusters.

    Args:
        graph1 (Dict): graph dictionary with `hks_clusterX` key where X is the number of cluster selected. If it is None
            is calculated. For this purpose graph must have also `id`, `ordered_graph`, `ordered_clusters`, `num_clusters`.
        graph2 (Dict): graph dictionary with `hks_clusterX` key where X is the number of cluster selected. If it is None
            is calculated. For this purpose graph must have also `id`, `ordered_graph`, `ordered_clusters`, `num_clusters`.
        clusters (Tuple[int, int]): tuple containing the index of the first and second cluster selected.

    Returns:
        List[List[int]]: auction algorithm result. The element index correspond to the first graph cluster's element and
            the list inside to the elements of the second graph cluster.
    """
    if graph1.get('hks_cluster{}'.format(clusters[0])) is None or \
            np.isnan(np.sum(graph1.get('hks_cluster{}'.format(clusters[0])))):
        graph1['hks_cluster{}'.format(clusters[0])] = calc_hks(graph1, clusters[0])
    if graph2.get('hks_cluster{}'.format(clusters[1])) is None or \
            np.isnan(np.sum(graph2.get('hks_cluster{}'.format(clusters[1])))):
        graph2['hks_cluster{}'.format(clusters[1])] = calc_hks(graph2, clusters[1])
    shape1 = graph1['hks_cluster{}'.format(clusters[0])].shape
    shape2 = graph2['hks_cluster{}'.format(clusters[1])].shape
    if len(shape1) == 1:
        shape1 = (1, shape1[0])
    if len(shape2) == 1:
        shape2 = (1, shape2[0])
    counts = (shape1[0], shape2[0])
    sim = np.empty(counts)
    for i in range(counts[0]):
        for j in range(counts[1]):
            sim[i, j] = 1 - cosine_distance(
                graph1['hks_cluster{}'.format(clusters[0])][i],
                graph2['hks_cluster{}'.format(clusters[1])][j])
    del graph1['hks_cluster{}'.format(clusters[0])]
    del graph2['hks_cluster{}'.format(clusters[1])]
    return auction_alg(sim, True)


def calc_hks(graph, cluster_idx, db: Database = None):
    hks = heat_kernel_signature(graph['ordered_graph'], 100)
    count = count_clusters(graph['ordered_clusters'])
    hks_filtered = hks[count[cluster_idx]['start']:count[cluster_idx]['end'] + 1, :]
    if db is not None:
        db.update_clusters(graph['id'], graph['num_clusters'], hks_clusters={
                           'hks_cluster{}'.format(cluster_idx): hks_filtered})
    return hks_filtered


def count_clusters(clusters):
    count = {}
    for idx in range(len(clusters)):
        if count.get(clusters[idx]):
            count[clusters[idx]]['end'] = idx
        else:
            count[clusters[idx]] = {'start': idx, 'end': idx}
    return count


def replace_cluster(clusters, graph1, graph2, node_correspondence, draw=False):
    adj_mat1, _, _, counter1 = group_clusters({
        'graph': graph1['ordered_graph'],
        'num_clusters': graph1['num_clusters'],
        'clusters': [c if c < clusters[0] else c - 1 if c != clusters[0] else graph1['num_clusters'] - 1 for c in graph1['ordered_clusters']]
    })
    adj_mat2, _, _, counter2 = group_clusters({
        'graph': graph2['ordered_graph'],
        'num_clusters': graph2['num_clusters'],
        'clusters': [c if c < clusters[1] else c - 1 if c != clusters[1] else graph2['num_clusters'] - 1 for c in graph2['ordered_clusters']]
    })
    new_adj_mat1 = add_one_cluster(adj_mat1, counter1, adj_mat2, counter2,
                                   flip_array(node_correspondence, counter2[-1][1]))
    new_adj_mat2 = add_one_cluster(adj_mat2, counter2, adj_mat1, counter1, node_correspondence)
    if draw:
        draw_graph(adj_mat1, counter1, title='Original 1')
        draw_graph(new_adj_mat1, counter1, counter2, title='Swapped 1')
        draw_graph(adj_mat2, counter2, title='Original 2')
        draw_graph(new_adj_mat2, counter2, counter1, title='Swapped 2')
    return (new_adj_mat1, new_adj_mat2) + make_new_clusters(clusters, graph1['ordered_clusters'], graph2['ordered_clusters'])


def add_one_cluster(adj_mat1, counter1, adj_mat2, counter2, node_correspondence):
    offset = counter1[-1]
    offset2 = counter2[-1]
    adj_mat1 = np.array(adj_mat1)
    adj_mat2 = np.array(adj_mat2)
    new_adj_mat = np.copy(adj_mat1[0:offset[0], 0:offset[0]])
    for i in range(len(node_correspondence)):
        new_adj_mat = np.vstack([new_adj_mat, merge_rows(node_correspondence[i], adj_mat1, offset)])
    for i in range(len(node_correspondence)):
        column = np.concatenate((new_adj_mat[offset[0] + i, :offset[0]], adj_mat2[offset2[0]:, offset2[0] + i]))
        new_adj_mat = np.column_stack([new_adj_mat, column])
    return new_adj_mat


def merge_rows(rows, adj_mat, offset):
    new_row = np.copy(create_row(rows[0] if rows else None, adj_mat, offset[0]))
    if rows is not None:
        for idx in range(1, len(rows)):
            new_row = np.bitwise_or(
                np.array(new_row, dtype=np.int32),
                np.array(create_row(rows[idx], adj_mat, offset[0]), dtype=np.int32)
            )
    return new_row


def create_row(row, adj_mat, offset):
    if row:
        return adj_mat[offset + row, 0:offset]
    return np.zeros((offset,))


def draw_graph(new_adj_mat, counter1, counter2=None, *, title):
    colors = ['red']
    partitions = []
    idx = 0
    for i in range(len(counter1)):
        if i == len(counter1) - 1 and counter2 is not None:
            break
        partitions.append(list(range(idx, idx + len(counter1[i][2]))))
        idx += len(counter1[i][2])
    if counter2 is not None:
        partitions.append(list(range(idx, idx + len(counter2[-1][2]))))
    draw_adjacency_matrix(new_adj_mat, partitions=[partitions], colors=colors, title=title)


def make_new_clusters(selected_clusters, old_clusters1, old_clusters2):
    clusters1 = [c if c < selected_clusters[0] else c - 1 for c in old_clusters1 if c != selected_clusters[0]]
    clusters1 += [clusters1[-1] + 1] * old_clusters2.count(selected_clusters[1])
    clusters2 = [c if c < selected_clusters[1] else c - 1 for c in old_clusters2 if c != selected_clusters[1]]
    clusters2 += [clusters2[-1] + 1] * old_clusters1.count(selected_clusters[0])
    return (clusters1, clusters2)


def make_new_graphs(db: Database, sim: Dict, num_clusters: int, verbose: bool = False):
    """Make the swap between two graphs with the most similar clusters.

    Args:
        db (Database): Database instance
        sim (Dict): Must contain 3 keys: `similarity` with the matrix of similarities between the clusters, `graph1_id` and `graph2_id`.
        num_clusters (int): Number of clusters of the graphs
        verbose (bool, optional): If to write messages. Defaults to False.
    """
    if not db.check_graph_id_augmented_cluster(sim['graph1_id'], sim['graph2_id']):
        # New graphs not present in database
        similarity = np.array(sim['similarity'])
        clusters = np.unravel_index(similarity.argmax(), similarity.shape)
        graph1 = db.get_graph_cluster(sim['graph1_id'], num_clusters, [
            'ordered_graph', 'ordered_clusters', 'num_clusters', 'g_class', 'hks_cluster{}'.format(clusters[0])])
        graph2 = db.get_graph_cluster(sim['graph2_id'], num_clusters, [
            'ordered_graph', 'ordered_clusters', 'num_clusters', 'g_class', 'hks_cluster{}'.format(clusters[1])])
        if verbose:
            print('Swapping clusters of graph 0 index {} with graph 1 index {}. Similarity of {:.4f}'.format(
                clusters[0], clusters[1], similarity[clusters]))
        couples = get_couples(graph1, graph2, clusters)
        replacement = replace_cluster(clusters, graph1, graph2, couples)
        node_labels1 = [graph1['ordered_graph'].nodes[idx] for idx, c in enumerate(graph1['ordered_clusters']) if c != clusters[0]] + \
            [graph2['ordered_graph'].nodes[idx] for idx, c in enumerate(graph2['ordered_clusters']) if c == clusters[1]]
        node_labels2 = [graph2['ordered_graph'].nodes[idx] for idx, c in enumerate(graph2['ordered_clusters']) if c != clusters[1]] + \
            [graph1['ordered_graph'].nodes[idx] for idx, c in enumerate(graph1['ordered_clusters']) if c == clusters[0]]
        new_graph1 = numpy_matrix_to_graph(replacement[0], graph1['ordered_graph'].graph, node_labels1)
        new_graph2 = numpy_matrix_to_graph(replacement[1], graph2['ordered_graph'].graph, node_labels2)
        db.save_augmented_clusters(sim['graph1_id'], sim['graph2_id'], num_clusters,
                                   graph=new_graph1, g_class=graph1['g_class'], clusters=replacement[2])
        db.save_augmented_clusters(sim['graph2_id'], sim['graph1_id'], num_clusters,
                                   graph=new_graph2, g_class=graph1['g_class'], clusters=replacement[3])


def save_hks(db: Database, graph):
    pool = ThreadPool(processes=5)
    threads = []
    for i in range(graph['num_clusters']):
        threads.append(pool.apply_async(calc_hks, (graph, i, db)))
    [res.get() for res in threads]
