import json
import gzip
import io
from typing import List, Dict, Tuple
import torch
import numpy as np
from augmentation.numpy_encoder import NumpyEncoder
from networkx.readwrite import json_graph
import networkx as nx


def adj_blob_to_graph(bytes_graph, compressed=False, is_array=False):
    """Convert blob data from database to a NetworkX graph

    Args:
        bytes_graph (bytes): bytes string to convert
        compressed (bool, optional): decompress string if it's compressed. Defaults to False.
        is_array (bool, optional): convert an array of graphs. Defaults to False.

    Returns:
        NetworkX graph
    """
    if bytes_graph is None:
        return None
    bytes_graph = io.BytesIO(bytes_graph).read()
    if compressed:
        bytes_graph = gzip.decompress(bytes_graph)
    jsoned_graph = json.loads(bytes_graph)
    if is_array:
        graphs = []
        for js_graph in jsoned_graph:
            graphs.append(json_graph.adjacency_graph(js_graph))
        return graphs
    return json_graph.adjacency_graph(jsoned_graph)


def graph_to_adj_blob(graph, compress=False, is_array=False):
    """Convert NetworkX graph to blob data for database use

    Args:
        graph (NetworkX Graph): graph to convert
        compress (bool, optional): compress graph. Defaults to False.
        is_array (bool, optional): convert an array of graphs. Defaults to False.

    Returns:
        bytes
    """
    if graph is None:
        return None
    if is_array:
        jsoned_graph = []
        for gr in graph:
            jsoned_graph.append(json_graph.adjacency_data(gr))
    else:
        jsoned_graph = json_graph.adjacency_data(graph)
    jsoned_graph = json.dumps(jsoned_graph, separators=(',', ':')).encode('utf-8')
    if compress:
        jsoned_graph = gzip.compress(jsoned_graph, 5)
    return io.BytesIO(jsoned_graph).read()


def torch_blob_to_graph(bytes_graph):
    """Convert torch blob data from database to a NetworkX graph

    Args:
        bytes_graph (bytes): bytes string to convert

    Returns:
        NetworkX graph
    """
    if bytes_graph is None:
        return None
    return torch.load(io.BytesIO(bytes_graph))


def graph_to_torch_blob(graph):
    """Convert NetworkX graph to torch blob data for database use

    Args:
        graph (NetworkX Graph): graph to convert

    Returns:
        bytes
    """
    if graph is None:
        return None
    buffer = io.BytesIO()
    torch.save(graph, buffer)
    buffer.seek(0)
    return buffer.read()


def np_array_to_blob(adj_mat, compress=False):
    """Convert numpy array (or matrix) to blob data for database use

    Args:
        adj_mat (numpy array): numpy array (or matrix) to convert
        compress (bool, optional): compress graph. Defaults to False.

    Returns:
        bytes
    """
    if adj_mat is None:
        return None
    jsoned_adj = json.dumps(adj_mat, cls=NumpyEncoder, separators=(',', ':')).encode('utf-8')
    if compress:
        jsoned_adj = gzip.compress(jsoned_adj, 5)
    return io.BytesIO(jsoned_adj).read()


def blob_to_np_array(bytes_adj, compressed=False):
    """Convert blob data from database to a numpy array numpy array (or matrix)

    Args:
        bytes_graph (bytes): bytes string to convert
        compressed (bool, optional): decompress string if it's compressed. Defaults to False.

    Returns:
        numpy array: numpy array
    """
    if bytes_adj is None:
        return None
    bytes_adj = io.BytesIO(bytes_adj).read()
    if compressed:
        bytes_adj = gzip.decompress(bytes_adj)
    jsoned_graph = json.loads(bytes_adj)
    return np.array(jsoned_graph)


def graph_to_numpy_matrix(
    graph: nx.Graph, sort_nodes_list: List[int] = None
) -> Tuple[np.ndarray, Dict, List[Dict], Dict[Tuple[int, int], Dict]]:
    """Convert a networkX graph into a numpy adjacency matrix. If `sort_nodes_list` is provided it sort the nodes.

    Args:
        graph (nx.Graph): graph to convert
        sort_nodes_list (List[int]): list of the new positions. Must be sequential from 0. Defaults to None.

    Returns:
        Tuple[np.ndarray, Dict, List[Dict], Dict[Tuple[int, int], Dict]]: return a tuple containing the adjacency matrix, the graph attributes, the nodes attributes and the edges attributes
    """
    if graph is None:
        return None
    sort_nodes_list = list(range(len(graph.nodes) - 1, -1, -1))
    mat = nx.to_numpy_array(graph, nodelist=sort_nodes_list)
    graph_attr = graph.graph
    node_attr = np.array([graph.nodes[n] for n in graph.nodes])
    edge_attr = np.array([graph.edges[e] for e in graph.edges])
    if sort_nodes_list is not None:
        node_attr = node_attr[sort_nodes_list]
        edge_attr = {(sort_nodes_list[e[0]], sort_nodes_list[e[1]]): edge_attr[idx]
                     for idx, e in enumerate(graph.edges)}
    return mat, graph_attr, node_attr, edge_attr


def numpy_matrix_to_graph(
    mat: np.ndarray,
    graph_attr: Dict = None,
    node_attr: List[Dict] = None,
    edge_attr: Dict[Tuple[int, int], Dict] = None
) -> nx.Graph:
    """Make a networkX graph from adjacency matrix and attributes.

    Args:
        mat (np.ndarray): adjacency matrix
        graph_attr (Dict, optional): graph attributes. Defaults to None.
        node_attr (List[Dict], optional): nodes attributes. Defaults to None.
        edge_attr (Dict[Tuple[int, int], Dict], optional): edges attributes. The dictionary must be with key the couple of nodes that define the edge. Defaults to None.

    Returns:
        nx.Graph: the built graph
    """
    if mat is None:
        return None
    graph = nx.from_numpy_array(mat)
    if graph_attr:
        graph.graph = graph_attr
    if node_attr is not None:
        for idx in range(len(graph.nodes)):
            for key in node_attr[idx].keys():
                graph.nodes[idx][key] = node_attr[idx][key]
    if edge_attr is not None:
        for e in graph.edges:
            attrKey = e if edge_attr.get(e) else e[::-1]
            for key in edge_attr[attrKey].keys():
                graph.edges[e][key] = edge_attr[attrKey][key]
    return graph
