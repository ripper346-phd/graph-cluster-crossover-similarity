from multiprocessing.pool import ThreadPool
from augmentation.database import Database


def generate_combination(similarity, num_clusters):
    sorted_sim = sorted([
        j for i in [
            [
                {'g2': idx, 'g1': idx2, 'val': val} for idx, val in enumerate(row)
            ] for idx2, row in enumerate(similarity)
        ]
        for j in i], key=lambda i: i['val'], reverse=True)
    comb = []
    subgraphs1 = []
    subgraphs2 = []
    sum = 0
    i = 0
    while len(comb) < num_clusters:
        if sorted_sim[i]['g1'] not in subgraphs1 and sorted_sim[i]['g2'] not in subgraphs2:
            comb.append(sorted_sim[i])
            subgraphs1.append(sorted_sim[i]['g1'])
            subgraphs2.append(sorted_sim[i]['g2'])
            sum += sorted_sim[i]['val']
        i += 1
    return (comb, sum / num_clusters)


def save_combination(db: Database, idx, sim):
    comb, avg = generate_combination(sim['similarity'], sim['num_clusters'])
    db.update_similarity(sim['graph1_id'], sim['graph2_id'], sim['num_clusters'], combination=comb, avg_combination=avg)
    if idx % 100 == 0:
        print(idx)


def save_combinations(db: Database, i=0):
    pool = ThreadPool(processes=150)
    while True:
        threads = []
        offset = 5000 * i
        data = db.get_graph_similarities(5000, offset, ['num_clusters', 'similarity'])
        if not data:
            break
        for idx, sim in enumerate(data):
            threads.append(pool.apply_async(save_combination, (db, idx + offset, sim)))
        i += 1
        [res.get() for res in threads]
