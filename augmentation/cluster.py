from augmentation.database import Database
import warnings
import numpy as np
from multiprocessing.pool import ThreadPool
from sklearn import cluster, mixture
from sklearn.neighbors import kneighbors_graph
import networkx as nx
from augmentation.graph_converter import graph_to_numpy_matrix, numpy_matrix_to_graph


def clusterize(adj_mat, cluster_type='spectral', params=[], n_clusters=3):
    np.random.seed(1)
    assert(adj_mat.shape[0] > n_clusters)

    # Cluster
    if cluster_type == 'mean shift':
        bandwidth = cluster.estimate_bandwidth(adj_mat, quantile=params['quantile'])
        sc = cluster.MeanShift(bandwidth=bandwidth, bin_seeding=True)
    elif cluster_type == 'two means':
        sc = cluster.MiniBatchKMeans(n_clusters=n_clusters)
    elif cluster_type == 'ward':
        connectivity = kneighbors_graph(adj_mat, n_neighbors=params['n_neighbors'], include_self=False)
        sc = cluster.AgglomerativeClustering(
            n_clusters=n_clusters, linkage='ward',
            connectivity=connectivity)
    elif cluster_type == 'spectral':
        sc = cluster.SpectralClustering(
            n_clusters=n_clusters, eigen_solver='arpack',
            affinity="precomputed")
    elif cluster_type == 'db scan':
        sc = cluster.DBSCAN(eps=params['eps'])
    elif cluster_type == 'optics':
        sc = cluster.OPTICS(min_samples=params['min_samples'],
                            xi=params['xi'],
                            min_cluster_size=params['min_cluster_size'])
    elif cluster_type == 'affinity propagation':
        sc = cluster.AffinityPropagation(
            damping=params['damping'], preference=params['preference'])
    elif cluster_type == 'average linkage ':
        connectivity = kneighbors_graph(adj_mat, n_neighbors=params['n_neighbors'], include_self=False)
        sc = cluster.AgglomerativeClustering(
            linkage="average", affinity="cityblock",
            n_clusters=n_clusters, connectivity=connectivity)
    elif cluster_type == 'birch':
        sc = cluster.Birch(n_clusters=n_clusters)
    elif cluster_type == 'gaussian mixture':
        sc = mixture.GaussianMixture(
            n_components=n_clusters, covariance_type='full')

    # catch warnings related to kneighbors_graph
    with warnings.catch_warnings():
        warnings.filterwarnings(
            "ignore",
            message="the number of connected components of the " +
            "connectivity matrix is [0-9]{1,2}" +
            " > 1. Completing it to avoid stopping the tree early.",
            category=UserWarning)
        warnings.filterwarnings(
            "ignore",
            message="Graph is not fully connected, spectral embedding" +
            " may not work as expected.",
            category=UserWarning)
    sc.fit(adj_mat)
    return sc


def make_subgraphs(graph, cluster_index_list, n_clusters):
    clusters = []
    for index_cluster in range(0, n_clusters):
        g_cl = graph.subgraph([index for index, element in enumerate(cluster_index_list) if element == index_cluster])
        nx.set_node_attributes(g_cl, {node: val for (node, val) in g_cl.degree()}, 'degree_labels')
        clusters.append(g_cl)
    return clusters


def compute_cluster(i, graph, n_clusters=3, db: Database = None, try_load=False, update=False, verbose=False, using_threads=False):
    cluster_labels = []
    # if using_threads and db:
    #     db = Database('DD', pool=True)
    if db and try_load:
        found_cluster = db.get_graph_cluster(i, n_clusters, ['clusters'])
        if found_cluster:
            cluster_labels = found_cluster.get('clusters')
    if not cluster_labels:
        try:
            clustered = clusterize(nx.adjacency_matrix(graph), n_clusters=n_clusters)
            cluster_labels = clustered.labels_
        except AssertionError as err:
            # Assertion error for graph not suitable for clustering. Do not save
            return (i, None)
        except Exception as err:
            raise err
        if db:
            db.save_clusters(i, n_clusters, graph=graph, clusters=cluster_labels, g_class=graph.graph['classes'][0])
    if verbose:
        print(cluster_labels)
    data_container = {'graph': graph, 'subgraphs': make_subgraphs(graph, cluster_labels, n_clusters), 'wl_res': {}}
    if update:
        db.update_clusters(i, n_clusters, subgraphs=data_container['subgraphs'])
    return (i, data_container)


def cluster_on_dataset(dataset, n_clusters=3, db: Database = None, try_load=False, update=False, verbose=False, use_threads=False):
    """Compute clusters for every graph in the dataset

    Args:
        dataset (networkX dataset): the dataset to compute
        n_clusters (int, optional): number of clusters to create. Defaults to 3.
        db (Database, optional): database instance. Defaults to None.
        try_load (bool, optional): try to load existent data. Defaults to False.
        update (bool, optional): update existent data with new computation. Defaults to False.
        verbose (bool, optional): Defaults to False.
        use_threads (bool, optional): Defaults to False.

    Returns:
        {[graph_id]: {'graph': networkXGraph, 'subgraphs': networkXGraph[], 'wl_res': {}}}
    """
    data_container = {}
    pool = ThreadPool(processes=16)
    threads = []
    for i, graph in enumerate(dataset):
        if use_threads:
            threads.append(pool.apply_async(compute_cluster, (i, graph,
                                                              n_clusters, db, try_load, update, verbose, True)))
        else:
            res = compute_cluster(i, graph, n_clusters, db, try_load, update, verbose)[1]
            if res is not None:
                data_container[i] = res
    if use_threads:
        thread_results = [res.get() for res in threads]
        for idx, res in thread_results:
            if res is not None:
                data_container[idx] = res
    return data_container


def group_clusters(data, calc_new_graph=False):
    """Group and rearrange clusters of graph

    Args:
        data ({'graph': networkX_graph, 'subgraphs': networkX_graph[], 'num_clusters': int, 'clusters': int[]}): graph data from database
        calc_new_graph (bool, optional): if rearrange graph and sort clusters list. Defaults to False.

    Returns:
        (numpy_matrix, networkX_graph, int[], int[]): adjacency matrix, graph, cluster division, full order of cluster division
    """
    counters = [[0, 0, []]]
    for i in range(1, data['num_clusters']):
        counters.append([data['clusters'].count(i - 1) + counters[-1][0], 0, []])
    sort_cluster = []
    for idx, val in enumerate(data['clusters']):
        sort_cluster.append(counters[val][0] + counters[val][1])
        counters[val][1] += 1
        counters[val][2].append(idx)
    adjacency_matrix, graph_attr, node_attr, edge_attr = graph_to_numpy_matrix(data['graph'], sort_cluster)
    if not calc_new_graph:
        return (adjacency_matrix, None, None, counters)
    graph = numpy_matrix_to_graph(adjacency_matrix, graph_attr, node_attr, edge_attr)
    return (adjacency_matrix, graph, sorted(data['clusters']), counters)


def group_all_clusters(data, db: Database = None):
    """Group, rearrange and update on db the clusters of each graph in dataset

    Args:
        data ({'id': int, 'num_clusters': int, 'graph': networkX_graph, 'clusters': int[]}[]): the data from database to compute
        db (Database, optional): database instance. Defaults to None.

    Returns:
        ({'id': int, 'num_clusters': int, 'graph': networkX_graph, 'clusters': int[], 'ordered_adj': numpy_matrix, 'ordered_graph': networkX_graph, 'ordered_clusters': int[]}[])
    """
    for i in range(len(data)):
        data[i]['ordered_adj'], data[i]['ordered_graph'], data[i]['ordered_clusters'], _ = group_clusters(data[i], True)
        if db:
            db.update_clusters(
                data[i]['id'],
                data[i]['num_clusters'],
                ordered_graph=data[i]['ordered_graph'],
                ordered_adj=data[i]['ordered_adj'],
                ordered_clusters=data[i]['ordered_clusters'])
    return data
