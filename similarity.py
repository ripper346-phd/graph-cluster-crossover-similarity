# %%
# Load dataset
import datetime
from augmentation.database import Database
from augmentation.similarity import similarity_on_clusters
from augmentation.in_ipynb import in_ipynb


def compute_similarity(table, data_container=None, verbose=False):
    db = Database(table, user='phd', passwd='cMRwE79hG5BxTfmj', db='graph_clustering_similarity',
                  host="localhost", port=45432, use_pool=True, pool_name="similarity")
    print(datetime.datetime.now(), 'compute similarity - start')
    if not data_container:
        data_container = db.get_clusters(['subgraphs', 'g_class'])
        print(datetime.datetime.now(), 'load complete')
    try:
        similarity_on_clusters(data_container, 5, db, try_load=True)
    except Exception as err:
        print(datetime.datetime.now(), 'finished with error')
        raise err
    print(datetime.datetime.now(), 'compute similarity - end')


if in_ipynb() and __name__ == "__main__":
    compute_similarity('DD', verbose=True)


# %%
# Save permutations
import datetime
from augmentation.database import Database
from augmentation.permutations import save_combinations
from augmentation.in_ipynb import in_ipynb


def compute_permutations(table):
    db = Database(table, user='phd', passwd='cMRwE79hG5BxTfmj', db='graph_clustering_similarity',
                  host="localhost", port=45432, use_pool=True, pool_name="permutations")
    print(datetime.datetime.now(), 'compute permutations - start')
    try:
        save_combinations(db)
    except Exception as err:
        print(datetime.datetime.now(), 'finished with error')
        raise err
    print(datetime.datetime.now(), 'compute permutations - end')


if in_ipynb() and __name__ == "__main__":
    compute_permutations('DD')

# %%
import sys

if __name__ == "__main__":
    if 'compute_similarity' in sys.argv:
        compute_similarity(sys.argv[-1])
    if 'compute_permutations' in sys.argv:
        compute_permutations(sys.argv[-1])
